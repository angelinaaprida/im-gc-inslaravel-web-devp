<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function welcome()
    {
        $namaDepan = "Your first name";
        $namaBelakang = "Your last name";

        return view('welcome', compact('namaDepan', 'namaBelakang'));
    }

	public function showRegistrationForm()
	{
		return view('register');
	}

    public function welcomePost(Request $request)
    {
        $namaDepan = $request->input('First_Name');
        $namaBelakang = $request->input('Last_Name');

        return view('welcome', compact('namaDepan', 'namaBelakang'));
    }

    public function processRegistration(Request $request)
    {
        $namaDepan = $request->input('nama_depan');
        $namaBelakang = $request->input('nama_belakang');

        return redirect('/welcome')->with(['nama_depan' => $namaDepan, 'nama_belakang' => $namaBelakang]);
    }
}